<?php
namespace LoicPennamen\StringService\Twig;

use LoicPennamen\StringService\StringService;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class StringServiceExtension extends AbstractExtension
{
	private $stringService;

	public function __construct(StringService $stringService)
	{
		$this->stringService = $stringService;
	}

	/**
	 * @return TwigFunction[]
	 */
	public function getFunctions()
	{
		return [
			new TwigFunction('translationExists', [$this, 'translationExists']),
			new TwigFunction('limitText', [$this, 'limitText']),
			new TwigFunction('toMoney', [$this, 'toMoney']),
			new TwigFunction('text', [$this, 'text']),
		];
	}

	/**
	 * @return TwigFilter[]
	 */
	public function getFilters()
	{
		return [
			new TwigFilter('limitText', [$this, 'limitText']),
			new TwigFilter('toMoney', [$this, 'toMoney']),
			new TwigFilter('text', [$this, 'text']),
		];
	}

	/**
	 * @param string $slug
	 * @param string|null $locale
	 * @return mixed
	 */
	public function translationExists(string $slug, string $locale = null)
	{
		return $this->stringService->translationExists($slug, $locale);
	}

	/**
	 * @param $str
	 * @param $length
	 * @param string $dots
	 * @return bool|string
	 */
	public function limitText($str, $length, $dots = '…')
	{
		return $this->stringService->limitText($str, $length, $dots);
	}

	/**
	 * @param $number
	 * @param string $currency
	 * @param bool $html
	 * @return string
	 */
	public function toMoney($number, $currency = '€', $html = true)
	{
		return $this->stringService->toMoney($number, $currency, $html);
	}

	/**
	 * @param $str
	 * @return string
	 */
	function text($str)
	{
		return $this->stringService->text($str);
	}
}
