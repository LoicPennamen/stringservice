<?php

namespace LoicPennamen\StringService;

use Symfony\Contracts\Translation\TranslatorInterface;

class StringService
{
	private $tl;
	
	public function __construct(TranslatorInterface $tl)
	{
		$this->tl = $tl;
	}

	/**
	 * @param int $minute
	 * @param int $hour
	 * @param int $day
	 */
	public function minuteHourDayToString($minute = 0, $hour = 0, $day = 0)
	{
		$strItems = [];
		$nMinute = $minute;
		$nHour = $hour;
		$nDay = $day;
		
		// TODO : voir si passer 8 heure en journée, ou 60 minutes en heure...
		// ...
		
		if($nDay)
			$strItems [] = $this->tl->trans('time.day', ['_n_' => $nDay]);
		if($nHour)
			$strItems [] = $this->tl->trans('time.hour', ['_n_' => $nHour]);
		if($nMinute)
			$strItems [] = $this->tl->trans('time.minute', ['_n_' => $nMinute]);
		
		if(!count($strItems))
			return null;
		
		return implode(', ', $strItems);
	}
	
	/**
	 * @param \DateTime|null $datetime
	 * @param string $format
	 * @return string
	 * @throws \Exception
	 */
	public function dateToHuman(\DateTime $datetime = null, $format = "l j F #")
	{
		/* formats :
		l Day name of week, plain
		D Day name of week, short
		j Day number, without leading zeros
		d Day number, with leading zeros
		m Month number with leading zeros
		n Month number without leading zeros
		F Month name in letters, full
		M Month name in letters, short
		Y Full year
		y Year on two chars
		# Year IF NOT current year
		*/
		$str = '';

		if($datetime == null)
			$datetime = new \DateTime();

		$arr = str_split($format);
		foreach ($arr as $char){
			switch($char){
				// Day name of week, plain
				case 'l':
//					$tmp_str = $this->tl->trans('dates.weekdays.'.$datetime->format('N'));
					$tmp_str = strftime('%A', $datetime->getTimestamp());
					break;
				// Day name of week, short
				case 'D':
//					$tmp_str = $this->tl->trans('dates.weekdays_short.'.$datetime->format('N'));
					$tmp_str = strftime('%a', $datetime->getTimestamp());
					break;
				// Month name in letters, full
				case 'F':
//					$tmp_str = $this->tl->trans('dates.months.'.$datetime->format('n'));
					$tmp_str = strftime('%B', $datetime->getTimestamp());
					break;
				// Month name in letters, short
				case 'M':
//					$tmp_str = $this->tl->trans('dates.months_short.'.$datetime->format('n'));
					$tmp_str = strftime('%b', $datetime->getTimestamp());
					break;
				// S rd...
				//case 'j':
					// Following BUGs in french for number 21 for instance
//					if($this->tl->getLocale() == 'en')
//						$tmp_str = $datetime->format($char) . $this->tl->trans('dates.'.$datetime->format('S'));
//					else
//						$tmp_str = $datetime->format($char);
//					break;
				// Special:
				case '#':
					if($datetime->format('Y') != date('Y'))
						$tmp_str = $datetime->format('Y');
					else
						$tmp_str = '';
					break;
				case ' ':
				case '.':
					$tmp_str = $char;
					break;
				default:
					$tmp_str = $datetime->format($char);
			}

			$str .= $tmp_str;
		}

		return mb_strtolower($str);
	}
	
	/**
	 * To HTML money string, with or without zeros
	 * @param $number
	 * @param string $currency
	 * @return string
	 */
	public function toMoney($number, $currency = '€', $html = true)
	{
		$space = $html ? "&nbsp;" : ' ';
		$str = (string) number_format($number, 2, ".", $space);
		
		// Remove empty decimals
		if(substr($str, -3) == '.00')
			$str = substr($str, 0, -3);
		
		return $str . $space . $currency;
	}
	
	/**
	 * @param \DateTime|null $dateTime
	 * @param bool $prefix
	 * @return string
	 * @throws \Exception
	 */
	public function smartDateToHuman(\DateTime $dateTime = null, $prefix = false)
	{
		if(!$dateTime)
			return '';
		
		$now = new \DateTime();
		
		// Today : Afficher l'heure uniquement
		if($now->format("Ymd") === $dateTime->format("Ymd"))
			return ($prefix ? $this->tl->trans('time.at') . ' ' : '') . $dateTime->format("H:i");
		
		// Not today, same year
		if($now->format("Y") === $dateTime->format("Y"))
			return ($prefix ? $this->tl->trans('time.on') . ' ' : '') . $this->dateToHuman($dateTime, "D j F");
		
		// Other year
		return ($prefix ? $this->tl->trans('time.on') . ' ' : '') . $this->dateToHuman($dateTime, "j M Y");
	}

	/**
	 * Number in full-readable letters
	 * @param $number
	 * @param string $locale
	 * @param bool $currency
	 * @param bool $decimalsAsCents
	 * @return bool|string
	 */
	public function numberToHuman($number, $locale = 'fr_FR', $currency = false, $decimalsAsCents = true)
	{
		$number = floatval($number);

		// Decimal don't look good, so...
		$numberAsStr = (string)$number;
		$numberAsArray = explode('.', $numberAsStr);
		$units = $numberAsArray[0];
		$decimals = isset($numberAsArray[1]) ? $numberAsArray[1] : null;

		// Make decimals always on base ten
		if($decimalsAsCents && strlen($decimals) == 1)
			$decimals .= '0';

		// Get texts
		$result_units =  numfmt_create($locale, \NumberFormatter::SPELLOUT)->format( $units );
		if($decimals)
			$decimalResult = numfmt_create($locale, \NumberFormatter::SPELLOUT)->format( $decimals );
		else
			$decimalResult = null;

		// Display as money
		if($currency){
			// units with currency
			$result = $result_units . ' ' . $currency . ($units > 1 ? 's' : '');
			// if decimals
			if($decimalResult)
				$result .= ' ' . $decimalResult;
		}

		// Casual display
		else{
			$result = $result_units;
			
			if($decimalResult){
				$commaStr = $this->tl->trans('number.comma');
				$result .= " $commaStr $decimalResult";
			}
		}

		return $result;
	}

	/**
	 * Get extension from string
	 * @param $filename
	 * @param bool $dot
	 * @return string
	 */
	public function getExt($filename, $dot = false)
	{
		return ($dot ? '.' : '') . strtolower(pathinfo($filename, PATHINFO_EXTENSION));
	}

	/**
	 * Get substring after last character
	 * @param $string
	 * @param string $separator
	 * @return bool|string
	 */
	public function getAfter($string, $separator = '/')
	{
		return substr($string, strrpos($string, $separator) + 1);
	}

	/**
	 * To folder-compatible name
	 * @param $str
	 * @return mixed|string
	 */
	public function toFileName($str, array $ignore = [])
	{
		$newStr = $str;

		// lowercase
		if(!in_array('lowercase', $ignore))
			$newStr = mb_strtolower($newStr);

		// no accents
		if(!in_array('accents', $ignore))
			$newStr = $this->removeAccents( $newStr );

		// Replace some characters with lines
		if(!in_array('spaces', $ignore))
			$newStr = preg_replace('/[\r\n\t ]+/', '-', $newStr);

		// Remove any characters other than:
		if(!in_array('others', $ignore))
			$newStr = preg_replace('/[^a-zA-Z0-9 \._-]+/', '', $newStr);

		// Prevent double lines
		$newStr = str_replace('--', '-', $newStr);

		// Make sure string is not empty
		return $newStr ? $newStr : '_';
	}

	/**
	 * Replace accents by equivalents
	 * @param $str
	 * @return string
	 */
	public function replaceAccents($str)
	{
		$replacements = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
			'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
			'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
			'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
			'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
		return strtr( $str, $replacements );
	}

	/**
	 * Get site host as string
	 * @param bool $lastSlash
	 * @return string
	 */
	public function getHost($lastSlash = true)
	{
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$domainName = $_SERVER['HTTP_HOST'] . ($lastSlash ? '/' : '');
		return $protocol.$domainName;
	}

	/**
	 * Remove all kinds of spaces
	 * @param $str
	 * @return mixed|string
	 */
	public function spaceless($str)
	{
		$str = trim( str_replace(' ', '', $str) );
		$str = str_replace("\n", '', $str);
		$str = str_replace("\t", '', $str);
		return $str;
	}

	/**
	 * Generate password
	 * TODO: upgrade by forcing special chars
	 * @param int $length
	 * @return string
	 */
	public function generatePassword($length = 8) {
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789____----';
		$count = mb_strlen($chars);

		for ($i = 0, $result = ''; $i < $length; $i++) {
			$index = rand(0, $count - 1);
			$result .= mb_substr($chars, $index, 1);
		}

		return $result;
	}

	/**
	 * Add suffix to file name
	 * @param $filename
	 * @param $suffix
	 * @return string
	 */
	public function suffixFileName($filename, $suffix){
		$ext = $this->getExt($filename, true);
		$base = substr($filename, 0, strlen($filename) - strlen($ext));
		return $base . $suffix . $ext;
	}

	/**
	 * Slug from string
	 * @param $str
	 * @return mixed|null|string|string[]
	 */
	public function slugify($str){
		$str = mb_strtolower($str);
		$str = str_replace(' ', '-', $str);
		$str = $this->spaceless($str);
		$str = $this->replaceAccents($str);
		$str = preg_replace('/[^a-z0-9\-]/', '', $str);
		return $str;
	}

	/**
	 * Any string, to camelCase
	 * @param $str
	 * @param bool $lcFirst
	 * @return mixed|null|string|string[]
	 */
	public function camelCase($str, $lcFirst=true){
		$str = trim($str);

		if(!$str)
			return '';

		$str = $this->replaceAccents($str);
		$str = strtolower($str);
		$str = preg_replace('/[^0-9A-Za-z+]+/', ' ', $str);
		$str = ucwords($str);
		$str = str_replace(' ', '', $str);

		if($lcFirst)
			$str = lcfirst($str);

		return $str;
	}

	/**
	 * Sanitize HTML for display with UTF-8 consideration and quotes encoding
	 * @param $str
	 * @return string
	 */
	public function sanitize($str){
		return htmlspecialchars((string)$str, ENT_QUOTES, 'UTF-8') ;
	}

	/**
	 * Cover multiple date formats
	 * @param $str
	 * @return bool|\DateTime|null
	 */
	public function strToDate($str)
	{
		$date = null;
		if(preg_match('/[\d]{2}\/[\d]{2}\/[\d]{4}/', $str))
			$date = \DateTime::createFromFormat('d/m/Y', $str);
		if(preg_match('/[\d]{4}\/[\d]{2}\/[\d]{2}/', $str))
			$date = \DateTime::createFromFormat('Y/m/d', $str);
		if(preg_match('/[\d]{2}-[\d]{2}-[\d]{4}/', $str))
			$date = \DateTime::createFromFormat('d-m-Y', $str);
		if(preg_match('/[\d]{4}-[\d]{2}-[\d]{2}/', $str))
			$date = \DateTime::createFromFormat('Y-m-d', $str);
		if(preg_match('/[\d]{8}/', $str))
			$date = \DateTime::createFromFormat('Ymd', $str);

		return $date;
	}

	/**
	 * Multiple date format to human readable
	 * @param $str
	 * @param string $format
	 * @return string
	 * @throws \Exception
	 */
	public function dateStrToHuman($str, $format = "l j F #")
	{
		$date = $this->strToDate($str);
		if($date){
			try {
				$str = $this->dateToHuman($date, $format);
			} catch (\Exception $exception) {
				// Keep original string
			}
		}
		return $str;
	}

	/**
	 * Neat way to limit text length
	 *
	 * @param $str
	 * @param $length
	 * @return bool|string
	 */
	public function limitText($str, $length, $dots = '…')
	{
		$str = trim($str);

		if(strlen($str) <= $length)
			return $str;

		$keep = substr($str, 0, $length);
		$firstBreak = strrpos($keep, ' ');

		if (false === $firstBreak)
			return $keep . $dots;

		return substr($keep, 0, $firstBreak) . $dots;
	}

	/**
	 * @param $slug
	 * @param null $locale
	 * @return mixed
	 */
	public function translationExists($slug, $locale = null)
	{
		if(!$locale)
			$locale = $this->tl->getLocale();
		$catalogue = $this->tl->getCatalogue($locale);

		return $catalogue->defines($slug);
	}

	/**
	 * @param $str
	 * @return string
	 */
	public function text($str)
	{
		return html_entity_decode(
			strip_tags(
				$str
			)
		);
	}
}
